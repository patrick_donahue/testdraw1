//
//  ViewController.swift
//  testdraw
//
//  Created by Patrick Donahue on 2/19/19.
//  Copyright © 2019 Patrick Donahue. All rights reserved.
//

import UIKit
import SpriteKit
import CoreML
import Vision

class ViewController: UIViewController {

    @IBOutlet weak var drawingPad: UIImageView!
    @IBOutlet weak var stencilView: UIImageView!
    @IBOutlet weak var wipeBtn: UIButton!
    @IBOutlet weak var tattooBtn: UIButton!
    @IBOutlet weak var spriteView: UIView!
    @IBOutlet weak var stencilBtn: UIButton!
    @IBOutlet weak var classificationLabel: UILabel!
    
    var lastPoint = CGPoint.zero
    var blueColor = UIColor(red: 0.0, green: 0.0, blue: 1.0, alpha: 1.0)
    var blackColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    var brushWidth: CGFloat = 10.0
    var opacity: CGFloat = 1.0
    var swiped = false
    var isErasing: Bool = false
    var isStenciling: Bool = true
    var isTattooing: Bool = false
    @IBOutlet weak var swallowImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tattooBtn.titleLabel?.text = "Tattooing"
        wipeBtn.titleLabel?.text = "Stenciling"
        isTattooing = true
        isStenciling = false
        drawingPad.frame = UIScreen.main.bounds
       // drawingPad.backgroundColor = .white
      //  swallowImage.backgroundColor = .white

        
//        let circlePath = UIBezierPath(arcCenter: CGPoint(x: 200,y: 400), radius: CGFloat(100), startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true)
//
//        let shapeLayer = CAShapeLayer()
//        shapeLayer.path = circlePath.cgPath
//
//        shapeLayer.fillColor = UIColor.clear.cgColor
//        shapeLayer.strokeColor = UIColor.blue.cgColor
//        shapeLayer.lineWidth = 20.0
//        stencilView.layer.addSublayer(shapeLayer)

    }
    
    @IBAction func decreaseColor(_ sender: Any) {
        isErasing = false
        isStenciling = false
        isTattooing = true
    }
    
    @IBAction func wipe(_ sender: Any) {
        isErasing = true
        isStenciling = false
        isTattooing = false
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        swiped = false
        if let touch = touches.first {
            lastPoint = touch.location(in: self.view)
        }
    }
    
    @IBAction func erase(_ sender: Any) {
        isErasing = false
        isStenciling = true
        isTattooing = false
    }
    
    @IBAction func scoreMe(_ sender: Any) {
//        if (stencilView!.image != nil) && drawingPad.image != nil {
//            print(compare(image1: stencilView.image!, image2: drawingPad.image!))
//        }
        
        //swallowImage.image = nil
 //       updateClassifications(for: drawingPad.image!)
        let image = resizeImage(image:#imageLiteral(resourceName: "new.png") , targetSize: CGSize(width: 250.0, height: 250.0))
        let widthInPixels = image.size.width * image.scale
        let heightInPixels = image.size.height * image.scale
        
        let newImage = resizeImage(image: drawingPad.image!, targetSize: CGSize(width: 250.0, height: 250.0))
        let widthInPixels2 = newImage.size.width * newImage.scale
        let heightInPixels2 = newImage.size.height * newImage.scale
        print(widthInPixels, heightInPixels)
//        print(widthInPixels2, heightInPixels2)
        print(drawingPad.image?.size.width, drawingPad.image?.size.height)
        let testimage = #imageLiteral(resourceName: "new.png")
        print(testimage.size.width, testimage.size.height)

        let test = compareImages(image1:image , image2: newImage)
        //This is used when the comparison extension is used.
        print(test?.count)
        
    }
    
    func pixelValues(fromCGImage imageRef: CGImage?) -> [UInt8]?
    {
        var width = 0
        var height = 0
        var pixelValues: [UInt8]?
        
        if let imageRef = imageRef {
            width = imageRef.width
            height = imageRef.height
            let bitsPerComponent = imageRef.bitsPerComponent
            let bytesPerRow = imageRef.bytesPerRow
            let totalBytes = height * bytesPerRow
            let bitmapInfo = imageRef.bitmapInfo
            
            let colorSpace = CGColorSpaceCreateDeviceRGB()
            var intensities = [UInt8](repeating: 0, count: totalBytes)
            
            let contextRef = CGContext(data: &intensities,
                                       width: width,
                                       height: height,
                                       bitsPerComponent: bitsPerComponent,
                                       bytesPerRow: bytesPerRow,
                                       space: colorSpace,
                                       bitmapInfo: bitmapInfo.rawValue)
            contextRef?.draw(imageRef, in: CGRect(x: 0.0, y: 0.0, width: CGFloat(width), height: CGFloat(height)))
            
            pixelValues = intensities
        }
        return pixelValues
    }
    
    //if not using data.difference and using return zip change return to Double?
    func compareImages(image1: UIImage, image2: UIImage) -> [UInt8]? {
        guard let data1 = pixelValues(fromCGImage: image1.cgImage),
            let data2 = pixelValues(fromCGImage: image2.cgImage),
            data1.count == data2.count else {
                
                return nil
        }
        
        let width = Double(image1.size.width)
        let height = Double(image1.size.height)
        
        //Uses the difference extension at the bottom
        return data2.difference(from: data1)
        
//        return zip(data1, data2)
//            .enumerated()
//            .reduce(0.0) {
//                $1.offset % 4 == 3 ? $0 : $0 + abs(Double($1.element.0) - Double($1.element.1))
//            }
//            * 100 / (width * height * 3.0) / 255.0
    }
    
    //Machine Learning
//    lazy var classificationRequest: VNCoreMLRequest = {
//        do {
//            /*
//             Use the Swift class `MobileNet` Core ML generates from the model.
//             To use a different Core ML classifier model, add it to the project
//             and replace `MobileNet` with that model's generated Swift class.
//             */
//            let model = try VNCoreMLModel(for: TattoosClassifier().model)
//
//            let request = VNCoreMLRequest(model: model, completionHandler: { [weak self] request, error in
//                self?.processClassifications(for: request, error: error)
//            })
//            request.imageCropAndScaleOption = .centerCrop
//            return request
//        } catch {
//            fatalError("Failed to load Vision ML model: \(error)")
//        }
//    }()
//
//    func updateClassifications(for image: UIImage) {
//        classificationLabel.text = "Classifying..."
//
//        let orientation = CGImagePropertyOrientation(rawValue: UInt32(image.imageOrientation.rawValue))!
//        guard let ciImage = CIImage(image: image) else { fatalError("Unable to create \(CIImage.self) from \(image).") }
//
//        DispatchQueue.global(qos: .userInitiated).async {
//            let handler = VNImageRequestHandler(ciImage: ciImage, orientation: orientation)
//            do {
//                try handler.perform([self.classificationRequest])
//            } catch {
//                /*
//                 This handler catches general image processing errors. The `classificationRequest`'s
//                 completion handler `processClassifications(_:error:)` catches errors specific
//                 to processing that request.
//                 */
//                print("Failed to perform classification.\n\(error.localizedDescription)")
//            }
//        }
//    }
//
//    func processClassifications(for request: VNRequest, error: Error?) {
//        DispatchQueue.main.async {
//            guard let results = request.results else {
//                print(request.results!)
//                return
//            }
//            // The `results` will always be `VNClassificationObservation`s, as specified by the Core ML model in this project.
//            let classifications = results as! [VNClassificationObservation]
//
//            if classifications.isEmpty {
//                print("empty")
//            } else {
//                // Display top classifications ranked by confidence in the UI.
//                let topClassifications = classifications.prefix(2)
//                let descriptions = topClassifications.map { classification in
//                    // Formats the classification for display; e.g. "(0.37) cliff, drop, drop-off".
//                    return String(format: "  (%.2f) %@", classification.confidence, classification.identifier)
//                }
//                print(descriptions)
//                self.classificationLabel.text = "Classification: " + descriptions.joined(separator: "\n")
//            }
//        }
//    }
    
    //First attempt at comparing images
//    func compare(image1: UIImage, image2: UIImage) -> Bool {
//        let data1: Data = image1.pngData()!
//        let data2: Data = image2.pngData()!
//        if data1 == data2 {
//            self.view.backgroundColor = .green
//            return true
//        } else {
//            self.view.backgroundColor = .red
//            return false
//        }
//    }
    
    func drawLineFrom(fromPoint: CGPoint, toPoint: CGPoint) {
        
        if isStenciling {
            UIGraphicsBeginImageContext(view.frame.size)
            let context = UIGraphicsGetCurrentContext()
            stencilView.image?.draw(in: CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height))
            
            context?.move(to: CGPoint(x: fromPoint.x, y: fromPoint.y))
            context?.addLine(to: CGPoint(x: toPoint.x, y: toPoint.y))
            
            brushWidth = 20.0
            context?.setLineWidth(brushWidth)
            context?.setStrokeColor(red: 0.0, green: 0.0, blue: 1.0, alpha: 1.0)
            context?.setLineCap(.round)
            context!.strokePath()
        
            stencilView.image = UIGraphicsGetImageFromCurrentImageContext()
            stencilView.alpha = opacity
            UIGraphicsEndImageContext()
        }
            
        else if(isErasing) {
            UIGraphicsBeginImageContext(view.frame.size)
            let context = UIGraphicsGetCurrentContext()
            stencilView.image?.draw(in: CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height))
            
            context?.move(to: CGPoint(x: fromPoint.x, y: fromPoint.y))
            context?.addLine(to: CGPoint(x: toPoint.x, y: toPoint.y))
            context?.setBlendMode(.hardLight)
            brushWidth = 25.0
            context?.setLineWidth(brushWidth)
            context?.setStrokeColor(red: 255.0, green: 255.0, blue: 255.0, alpha: 0.20)
            context?.setLineCap(.square)
            context!.strokePath()
            stencilView.image = UIGraphicsGetImageFromCurrentImageContext()
            stencilView.alpha = opacity
            UIGraphicsEndImageContext()
        }
        
        else if isTattooing {
            UIGraphicsBeginImageContext(view.frame.size)
            let context = UIGraphicsGetCurrentContext()
            drawingPad.image?.draw(in: CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height))
            
            context?.move(to: CGPoint(x: fromPoint.x, y: fromPoint.y))
            context?.addLine(to: CGPoint(x: toPoint.x, y: toPoint.y))

//            let sk: SKView = SKView()
//            sk.frame = CGRect(x: toPoint.x-50, y: toPoint.y-75, width: 100, height: 100)
//            sk.backgroundColor = .clear
//            self.view.addSubview(sk)
//
//            let scene: SKScene = SKScene(size: self.view.bounds.size)
//            scene.scaleMode = .fill
//            scene.backgroundColor = .clear
//
//            let en = SKEmitterNode(fileNamed: "MyParticle.sks")
//            en?.position = sk.center
//            en?.particleColorBlendFactorSpeed = 2
//            scene.addChild(en!)
//            sk.presentScene(scene)

            context?.setBlendMode(.normal)
            brushWidth = 400.0
            context?.setLineCap(.round)
            context?.setLineWidth(brushWidth)
            context?.setStrokeColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
            context!.strokePath()
            
            drawingPad.image = UIGraphicsGetImageFromCurrentImageContext()
            drawingPad.alpha = opacity
            UIGraphicsEndImageContext()
        }
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        swiped = true
        if let touch = touches.first {
            let currentPoint = touch.location(in: view)
            drawLineFrom(fromPoint: lastPoint, toPoint: currentPoint)

            lastPoint = currentPoint
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if !swiped {
            drawLineFrom(fromPoint: lastPoint, toPoint: lastPoint)
            print(lastPoint)
        }
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
//        var newSize: CGSize
//        if(widthRatio > heightRatio) {
//            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
//        } else {
//            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
//        }
        
        let newSize = CGSize(width: targetSize.width, height: targetSize.height)
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}

extension Array where Element: Hashable {
    func difference(from other: [Element]) -> [Element] {
        let thisSet = Set(self)
        let otherSet = Set(other)
        return Array(thisSet.symmetricDifference(otherSet))
    }
}
